<?php 

/*
 *
 */
class printFilesClass{

    public function printFiles(){

        $sort = pathParamsClass::getSort();
        $path = pathParamsClass::getPath();
        $files = pathParamsClass::getFiles();

		$route = Helper::getRoute();
		
		if ($route === 'size'){
			$files = Helper::sortBySize($files);
		}
		
		if ($route === 'type'){
			$files = Helper::sortByType($files);
		}
		
		if ($route === 'name'){
			$files = Helper::sortByName($files);
		}
		
		// print_r($files);
		// exit;
		
        //распечатка результатов		
		include(ROOT.'/view/page.php');

    }

}