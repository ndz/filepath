<?php

class Helper {

	public static function getRoute(){
		
		if (!empty($_SERVER['REQUEST_URI'])){
			
			return trim($_SERVER['REQUEST_URI'], '/');
		}
	}
	
	
	public static function sortByName($file){
		
		$array_lowercase = array_map('strtolower', $file['name']);
		array_multisort($array_lowercase, SORT_ASC, SORT_STRING, $file['name']);
		
		return $file;
	}
	
	
	public static function sortByType($file){
		array_multisort($file["type"], SORT_DESC, SORT_STRING, $file["size"], $file["name"]);
		
		return $file;
	}
	
	public static function sortBySize($file){
		array_multisort($file["size"], SORT_ASC, $file["type"], $file["name"]);
			
		return $file;
	}
}