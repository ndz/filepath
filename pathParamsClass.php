<?php
class pathParamsClass{

    private static $sort = '';
    private static $path = false;
    private static $stObj = null;
    private static $files = [];

	
    private function __construct(){}
	
    private function __clone(){}	

	
    public static function setFiles($files){

        if (is_array($files) && sizeof($files) > 0){

            foreach($files as $number => $file){
					
					static::$files['name'][] = $file;
					static::$files['size'][] = filesize($file);
					static::$files['type'][] = filetype($file);
            }
        }
    }

	
    public static function getFiles(){
        return !empty(static::$files) ? static::$files : false;
    }

	
    public static function getInstance(){

        if (!is_object(static::$stObj)){
            static::$stObj = new pathParamsClass();
        }

        return static::$stObj;
    }

	
    public static function setSort($sort = 'name'){
        if (in_array($sort, ['name', 'type', 'size'])){
            static::$sort = $sort;
            return true;
        }
    }

	
    public static function getSort(){
        if (in_array(static::$sort, ['name', 'type', 'size'])){
            return static::$sort;
        } else {
            return false;
        }
    }

	
    public static function setPath($path){
        if (file_exists($path) && is_dir($path)){
            static::$path = $path;
            return true;
        }
    }

	
    public static function getPath(){
        return (file_exists(static::$path) && is_dir(static::$path)) ? static::$path : false;
    }

}