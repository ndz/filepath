<?php 

class SitePage {
	
	public static function renderPageByRoute($route, $getFiles, $printFiles){
		
		if ( in_array($route, ['size', 'name', 'type']) ){
			
			pathParamsClass::setSort($route);
			pathParamsClass::setPath($_SERVER['DOCUMENT_ROOT']);
			
			$getFiles->getFiles();
			$printFiles->printFiles();
	
			return true;
		} else {
			exit("Route not found!!!");
		}
		
	}
}