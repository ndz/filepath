<?php
// Общие настройки
ini_set('max_execution_time', 999);
ini_set('display_errors', 1);
error_reporting(E_ALL);

// Подключение файлов системы
define('ROOT', dirname(__FILE__));


//var_dump(ROOT.'/FilePage.php');
include(ROOT.'/Helper.php');
include(ROOT.'/pathParamsClass.php');
include(ROOT.'/getFilesClass.php');
include(ROOT.'/SitePage.php');
include(ROOT.'/printFilesClass.php');

$route = Helper::getRoute();

$getFiles = new getFilesClass();
$printFiles = new printFilesClass();

pathParamsClass::getInstance();



SitePage::renderPageByRoute($route, $getFiles, $printFiles);


// //в жизни этот синглтон можно передавать между несколькими классами,
// //как неявные параметры, это будет замена для глобальных переменных
// //и гарантировать целостность и верность передаваемых данных